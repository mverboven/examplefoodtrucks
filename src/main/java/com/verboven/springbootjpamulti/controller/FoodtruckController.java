package com.verboven.springbootjpamulti.controller;

import com.verboven.springbootjpamulti.entity.Event;
import com.verboven.springbootjpamulti.entity.Foodtruck;
import com.verboven.springbootjpamulti.exception.ResourceNotFoundException;
import com.verboven.springbootjpamulti.repository.FoodtruckRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collection;

@RestController
public class FoodtruckController {

    @Autowired
    private FoodtruckRepository foodtruckRepository;

    // Get all foodtrucks
    @GetMapping("/foodtrucks")
    public Collection<Foodtruck> getAllFoodtrucks(){
        return foodtruckRepository.findAll();
    }

    // Get a single foodtruck by id
    @GetMapping("/foodtrucks/{id}")
    public Foodtruck getFoodtruckbyId(@PathVariable Long id){
        return foodtruckRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Foodtruck", "id", id));
    }

    // Get a single foodtruck's events by id
    @GetMapping("/foodtrucks/{id}/events")
    public Collection<Event> getFoodtruckEvents(@PathVariable Long id) {
        Foodtruck foodtruck = foodtruckRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Foodtruck", "id", id));

        return foodtruck.getEvents();
    }

    // Get the participation fee of a single foodtruck by id
    @GetMapping("/foodtrucks/{id}/fee")
    public ResponseEntity<String> getFoodtruckFee(@PathVariable Long id) {
        Foodtruck foodtruck = foodtruckRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Foodtruck", "id", id));

        return ResponseEntity.status(HttpStatus.OK)
                .body("Your fee is €" + (foodtruck.getStaffnumber()*10));
    }

    // Create a new foodtruck
    @PostMapping("/foodtrucks")
    public Foodtruck createFoodtruck(@Valid @RequestBody Foodtruck foodtruck){
        return foodtruckRepository.save(foodtruck);
    }

    // Delete a foodtruck
    @DeleteMapping("/foodtrucks/{id}")
    public ResponseEntity<?> deleteFoodtruck(@PathVariable Long id) {
        Foodtruck foodtruck = foodtruckRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Foodtruck", "id", id));

        foodtruckRepository.delete(foodtruck);

        return ResponseEntity.ok().build();
    }

    // Update a foodtruck
    @PutMapping("/foodtrucks/{id}")
    public Foodtruck updateFoodtruck(@Valid @RequestBody Foodtruck foodtruckDetails, @PathVariable Long id) {

        Foodtruck foodtruck = foodtruckRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Foodtruck", "id", id));

        foodtruck.setName(foodtruckDetails.getName());
        foodtruck.setStaffnumber(foodtruckDetails.getStaffnumber());
        foodtruck.setProducts(foodtruckDetails.getProducts());
        foodtruck.setEvents(foodtruckDetails.getEvents());

        return foodtruckRepository.save(foodtruck);
    }

}
