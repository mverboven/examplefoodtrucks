package com.verboven.springbootjpamulti.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "events")
public class Event {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "event_id")
    private Long id;

    @NotNull
    private String name;

    @NotNull
    private String location;

    @Column(name="event_date")
    @JsonFormat(pattern = "YYYY-MM-dd")
    private LocalDate date;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "foodtrucks_events",
            joinColumns = @JoinColumn(name = "event_id", referencedColumnName = "event_id"),
            inverseJoinColumns = @JoinColumn(name = "foodtruck_id", referencedColumnName = "foodtruck_id"))
    private Set<Foodtruck> foodtrucks = new HashSet<Foodtruck>();

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Set<Foodtruck> getFoodtrucks() {
        return foodtrucks;
    }

    public void setFoodtrucks(Set<Foodtruck> foodtrucks) {
        this.foodtrucks = foodtrucks;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void addFoodtruck(Foodtruck foodtruck){
        foodtruck.getEvents().add(this);
        foodtrucks.add(foodtruck);
    }

    public void removeFoodtruck(Foodtruck foodtruck){
        foodtrucks.remove(foodtruck);
        foodtruck.getEvents().remove(this);
    }
}
